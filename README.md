

# laravel admin login

## Paso 1: Instalación de Laravel

**Si aún no has instalado Laravel, puedes hacerlo utilizando Composer:**
composer create-project --prefer-dist laravel/laravel nombre-del-proyecto

## Paso 2: Configuración de la base de datos

Configura tu base de datos en el archivo .env:

    DB_CONNECTION=mysqz
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=nombre_de_la_base_de_datos
    DB_USERNAME=tu_usuario
    DB_PASSWORD=tu_contraseña

## Paso 3: Crear el modelo y la migración para los administradores

Genera el modelo y la migración para los administradores:

php artisan make:model Admin -m

Luego, edita la migración creada en database/migrations/xxxx_xx_xx_create_admins_table.php para definir los campos necesarios:

  

    php
    
    Copiar código
    
    public function up()
    
    {
    
    Schema::create('admins', function (Blueprint $table) {
    
    $table->id();
    
    $table->string('name');
    
    $table->string('email')->unique();
    
    $table->string('password');
    
    $table->timestamps();
    
    });
    
    }



php artisan migrate

## Paso 4: Configuración del sistema de autenticación

Vamos a utilizar Laravel Breeze para simplificar la configuración de autenticación.
Instala Laravel Breeze:

**composer require laravel/breeze --dev**


php artisan breeze:install

npm install && npm run dev

## Paso 5: Crear el guard y provider para administradores

**Configura el guard y provider en config/auth.php:**

  

    php
    
    Copiar código
    
    'guards' => [
    
    'admin' => [
    
    'driver' => 'session',
    
    'provider' => 'admins',
    
    ],
    
    // otros guards...
    
    ],
    
      
    
    'providers' => [
    
    'admins' => [
    
    'driver' => 'eloquent',
    
    'model' => App\Models\Admin::class,
    
    ],
    
    // otros providers...
    
    ],

## Paso 6: Crear las rutas y controladores para administradores

**Crea un controlador para la autenticación de administradores:**

php artisan make:controller AdminAuthController

Luego, define las rutas para el login de administradores en routes/web.php:

  

    php
    
    
    
    use App\Http\Controllers\AdminAuthController;
    
      
    
    Route::prefix('admin')->group(function () {
    
    Route::get('login', [AdminAuthController::class, 'showLoginForm'])->name('admin.login');
    
    Route::post('login', [AdminAuthController::class, 'login']);
    
    Route::post('logout', [AdminAuthController::class, 'logout'])->name('admin.logout');
    
    });

## Paso 7: Implementar métodos en el controlador de administradores

*Edita el controlador AdminAuthController para manejar el login de administradores:*

  

    php
    
    Copiar código
    
    namespace App\Http\Controllers;
    
      
    
    use Illuminate\Http\Request;
    
    use Illuminate\Support\Facades\Auth;
    
      
    
    class AdminAuthController extends Controller
    
    {
    
    public function showLoginForm()
    
    {
    
    return view('admin.login');
    
    }
    
      
    
    public function login(Request $request)
    
    {
    
    $credentials = $request->only('email', 'password');
    
      
    
    if (Auth::guard('admin')->attempt($credentials)) {
    
    return redirect()->intended('/admin/dashboard');
    
    }
    
      
    
    return back()->withErrors([
    
    'email' => 'The provided credentials do not match our records.',
    
    ]);
    
    }
    
      
    
    public function logout(Request $request)
    
    {
    
    Auth::guard('admin')->logout();
    
      
    
    return redirect()->route('admin.login');
    
    }
    
    }

## Paso 8: Crear la vista de login para administradores

Crea la vista resources/views/admin/login.blade.php:

  

    html
    
    Copiar código
    
    <!DOCTYPE  html>
    
    <html  lang="en">
    
    <head>
    
    <meta  charset="UTF-8">
    
    <meta  name="viewport"  content="width=device-width, initial-scale=1.0">
    
    <title>Admin Login</title>
    
    </head>
    
    <body>
    
    <form  method="POST"  action="{{ route('admin.login') }}">
    
    @csrf
    
    <div>
    
    <label  for="email">Email:</label>
    
    <input  type="email"  name="email"  id="email"  required>
    
    </div>
    
    <div>
    
    <label  for="password">Password:</label>
    
    <input  type="password"  name="password"  id="password"  required>
    
    </div>
    
    <button  type="submit">Login</button>
    
    </form>
    
    </body>
    
    </html>

## Paso 9: Middleware para rutas de administradores

Crea un middleware para proteger las rutas de administradores:

  



**php artisan make:middleware AdminMiddleware**

**Luego, edita el middleware AdminMiddleware en app/Http/Middleware/AdminMiddleware.php:**

  

    php
    
    Copiar código
    
    namespace App\Http\Middleware;
    
      
    
    use Closure;
    
    use Illuminate\Http\Request;
    
    use Illuminate\Support\Facades\Auth;
    
      
    
    class AdminMiddleware
    
    {
    
    public function handle(Request $request, Closure $next)
    
    {
    
    if (!Auth::guard('admin')->check()) {
    
    return redirect()->route('admin.login');
    
    }
    
      
    
    return $next($request);
    
    }
    
    }

**Registra el middleware en app/Http/Kernel.php:**

  

    php
    
    Copiar código
    
    protected $routeMiddleware = [
    
    // otros middlewares...
    
    'admin' => \App\Http\Middleware\AdminMiddleware::class,
    
    ];

## Paso 10: Protege las rutas de administradores

En routes/web.php, protege las rutas que requieren autenticación de administrador:

  

    php
    
    Copiar código
    
    Route::prefix('admin')->middleware('admin')->group(function () {
    
    Route::get('dashboard', function () {
    
    return view('admin.dashboard');
    
    });
    
    });

Conclusión

Con estos pasos, has configurado un sistema de login para administradores en Laravel. Puedes personalizar aún más las vistas, controladores y middleware según tus necesidades.


